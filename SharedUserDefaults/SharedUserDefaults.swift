
//  SharedUserDefaults.swift
//  Created by Khaled El Ammari on 11/03/2020.


import Foundation

struct SharedUserDefaults {
    static let suiteName = "group.com.Bmad.SharedUserDefaults" 
    
    struct Keys {
        static let passedString = "passedString"
        static let passedId = "001"
    }
}
