
//  SharedUserDefaults
//  Created by Khaled El Ammari on 11/03/2020.
//

import UIKit

let sharedUserDefaults = UserDefaults(suiteName: SharedUserDefaults.suiteName)

class ViewController: UIViewController {
  var passedString = ""
 var passedArray: [String] = []
    var lb = UILabel()
    
    
    
   var lbtxtField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        self.view.backgroundColor = .white
        /////////////Optional/////////
        passedArray = ["Ojii1","Ojii2","Ojii3","Ojii4","Ojii5"]
        /////////////Optional/////////
       // Adding Text
       lb = UILabel(frame: CGRect(x: 100, y: 200, width: 200, height: 50))
       // lb.center = view.center
     
        lb.text = passedString //"anything"
        self.view.addSubview(lb)
 
        // Adding TextField
         lbtxtField = UITextField(frame: CGRect(x: 100, y: 300, width: 200, height: 50))
        lbtxtField.backgroundColor = .lightGray
         //lbtxtField.center = view.center
       // lbtxtField.text = sharedUsername //"anything"
        self.view.addSubview(lbtxtField)
        
        
        let button = UIButton(frame: CGRect(x: 100, y: 400, width: 100, height: 50))
         button.backgroundColor = .lightGray
         button.setTitle("Send", for: .normal)
         button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

         self.view.addSubview(button)
      
        
        passedString =  lb.text!
        
   
   /////////////Optional/////////
        let ArrayString = passedArray.joined(separator: "\", \"")
        print(ArrayString)
     /////////////Optional/////////
    }

    @objc func buttonAction(sender: UIButton!) {
    
       
        passedString =  lbtxtField.text!
        
        
        
       // sharedUsername = lbtxtField.text!
        //  print(passedString)
        
        sharedUserDefaults?.set(passedString, forKey: SharedUserDefaults.Keys.passedString)
                 guard let sharedUsername = sharedUserDefaults?.string(forKey: SharedUserDefaults.Keys.passedString) else { return }
                 print(" app Shared username: \(sharedUsername)")
       lb.text = "\(sharedUsername) : sent to iMessage extention!"
        
        let ID = "00000324"
           sharedUserDefaults?.set(ID, forKey: SharedUserDefaults.Keys.passedId)
                      guard let sharedUserID = sharedUserDefaults?.string(forKey: SharedUserDefaults.Keys.passedId) else { return }
        print(sharedUserID)
    }
}

